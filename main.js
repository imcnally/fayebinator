var treeWalker = document.createTreeWalker(
  document.body,
  NodeFilter.SHOW_TEXT,
  {acceptNode : function(){return NodeFilter.FILTER_ACCEPT}},
  false
);

var wordsToReplace = [
  'baby',
  'maybe'
];

var wordRegex = new RegExp('(' + wordsToReplace.join('|') + ')', 'gi');

while (treeWalker.nextNode())
  treeWalker.currentNode.nodeValue = treeWalker.currentNode.nodeValue.replace(wordRegex, 'Faye Bi');