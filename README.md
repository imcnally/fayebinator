# Fayebinator

A Chrome extension that changes every instance of a word that sounds like Faye Bi's name to Faye Bi, i.e., "maybe", "baby".

### Installation

1. Download the [zip file](https://github.com/imcnally/fayebinator/archive/master.zip) or clone repository.

2. In Chrome, go to Extensions (visit chrome://extensions).

3. Select "Load unpacked extension...".

4. Select this project folder.

5. Enjoy

### Example

![Alt text](https://raw.githubusercontent.com/imcnally/fayebinator/master/example/call_me_fayebi.jpg)